<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['as' => 'company.index', 'uses' => 'CompanyController@index']);
Route::get('/company/{symbol}/show', ['as' => 'company.show', 'uses' => 'CompanyController@show']);
Route::get('/company/{symbol}/get-quote', ['as' => 'company.getQuote', 'uses' => 'CompanyController@getQuote']);
Route::post('/company/save-quote', ['as' => 'company.saveQuote', 'uses' => 'CompanyController@saveQuote']);
Route::get('/company/{symbol}/get-directors', ['as' => 'company.getDirectors', 'uses' => 'CompanyController@getDirectors']);
