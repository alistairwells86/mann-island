@extends('layout.app')

@section('content')
    <p><a href="{{route('company.index')}}">Back to Companies List</a>
    <hr>
    <p>{{$symbol}}</p>
    <div><a href="{{route('company.getQuote', ['symbol' => $symbol])}}">Quotes & Quote History</a></div>
    <div><a href="{{route('company.getDirectors', ['symbol' => $symbol])}}">List of Company Directors</a></div>

@endsection
