@extends('layout.app')

@section('content')
    <p>This list is populated from the result of <code>getCompanies()</code>. The links below will allow you to call the other functions of the web service.</p>
    @foreach($companies as $company)
        <div><a href="{{route('company.show', ['symbol' => $company['symbol']])}}">{{$company['name']}}</a></div>
    @endforeach

@endsection
