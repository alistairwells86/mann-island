@extends('layout.app')

@section('content')

    @if($quote)
        <p><a href="{{route('company.index')}}">Back to Companies List</a>
        <hr>
        <h4>Current Price for {{$symbol}}</h4>
        <p>{{$quote}}</p>
        {!! Form::open(['route' => 'company.saveQuote']) !!}
        <input type="hidden" name="price" value="{{$quote}}">
        <input type="hidden" name="symbol" value="{{$symbol}}">
        <button type="submit" class="btn btn-sm btn-default">Save This Quote</button>
        {!! Form::close() !!}
        <hr>
        <h4>Price History</h4>
        @if($quote_history->count() > 0)
            <table class="table">
                <thead>
                <tr>
                    <th>Price</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quote_history as $quote)
                    <tr>
                        <td>
                            {{$quote->price}}
                        </td>
                        <td>
                            {{$quote->created_at->format('d/m/Y')}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>No saved quotes</p>

        @endif


    @endif

@endsection
