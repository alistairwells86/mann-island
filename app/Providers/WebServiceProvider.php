<?php

namespace App\Providers;

use App\Interfaces\WebServiceInterface;
use Illuminate\Support\ServiceProvider;

class WebServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WebServiceInterface::class, config('services.web_service.class'));
    }
}
