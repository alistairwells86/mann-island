<?php

namespace App\Services;

use \Exception;
use SoapClient;
use SoapFault;
use SoapVar;
use App\Interfaces\WebServiceInterface;
use App\Interfaces\OutputInterface;

class Soap implements WebServiceInterface, OutputInterface
{
    public $soap_client;
    public $soap_url = 'http://localhost:8000/server.php?wsdl';
    private $username = 'php-exercise@mannisland.co.uk';
    private $password = 'p455w0rd';

    /**
     * Loads the values from the .env file, falling back to the class properties
     */
    public function __construct()
    {
        $this->soap_url = env("SOAP_URL", $this->soap_url);
        $this->username = env("USERNAME", $this->username);
        $this->password = env("PASSWORD", $this->password);
        $this->initialiseClient();
    }


    /**
     * Sets up the SoapClient object ready for calls
     *
     * @throws Exception
     */
    protected function initialiseClient()
    {
        try {
            $this->soap_client = new SoapClient($this->soap_url);
        } catch (Exception $e) {
            throw new Exception("Couldn't initialise the SoapClient", 0, $e);
        }
    }


    /**
     * Build the XML structure for the authentication elements
     *
     * @return SoapVar
     * @throws Exception
     */
    public function buildAuthentication()
    {
        try {
            return new SoapVar("<authentication><username>{$this->username}</username><password>{$this->password}</password></authentication>", XSD_ANYXML);
        } catch (Exception $e) {
            throw new Exception("Error building authentication XML", 0, $e);
        }
    }


    /**
     * Gets an array of Companies from the getCompanies SOAP function
     *
     * @return array
     * @throws Exception
     */
    public function getCompanies()
    {
        try {
            return $this->outputCompanies($this->soap_client->getCompanies($this->buildAuthentication()));
        } catch (Exception $e) {
            throw new Exception("Error performing the getCompanies function", 0, $e);
        }

    }

    /**
     * Returns the companies in a array
     *
     * @param $data
     * @return array
     * @throws Exception
     */
    public function outputCompanies($data)
    {
        try {
            return collect($data->item)->toArray();
        } catch (Exception $e) {
            throw new Exception("Error performing the outputCompanies function", 0, $e);
        }

    }


    /**
     * Get an stock price of the the company passed via $symbol
     *
     * @param $symbol
     * @return float
     * @throws Exception
     */
    public function getQuote($symbol)
    {
        try {
            $symbol = strtoupper($symbol);
            $symbol_request = new SoapVar("<symbol>{$symbol}</symbol>", XSD_ANYXML);
            return $this->outputQuote($this->soap_client->getQuote($this->buildAuthentication(), $symbol_request));
        } catch (Exception $e) {
            throw new Exception("Error performing the getQuote function", 0, $e);
        }
    }


    /**
     * Outputs the share price as a float
     *
     * @param $data
     * @return float
     */
    public function outputQuote($data)
    {
        return (float)$data;
    }


    /**
     * Gets an array of the company directors
     *
     * @param $symbol
     * @return array
     * @throws Exception
     */
    public function getDirectorsBySymbol($symbol)
    {
        try {
            $symbol = strtoupper($symbol);
            $symbol_request = new SoapVar("<symbol>{$symbol}</symbol>", XSD_ANYXML);
            return $this->outputDirectorsBySymbol($this->soap_client->getDirectorsBySymbol($this->buildAuthentication(), $symbol_request));
        } catch (SoapFault $e) {
            throw new Exception('There is an error with the getCompanyDirectorsBySymbol SOAP function', 0, $e);
        } catch (Exception $e) {
            throw new Exception('There is a general error when executing the getCompanyDirectorsBySymbol function', 0, $e);
        }
    }


    /**
     * Returns the company directors in an array
     *
     * @param $data
     * @return array
     * @throws Exception
     */
    public function outputDirectorsBySymbol($data)
    {
        try {
            return collect($data)->toArray();
        } catch (Exception $e) {
            throw new Exception('Error building array of directors', 0, $e);
        }
    }


    /**
     * Change the password at runtime
     *
     * @param mixed|string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     *
     * Change the username at runtime
     *
     * @param mixed|string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }


    /**
     * Change the SOAP url at runtime
     *
     * @param mixed|string $soap_url
     */
    public function setSoapUrl($soap_url)
    {
        $this->soap_url = $soap_url;
        $this->initialiseClient();
    }

}