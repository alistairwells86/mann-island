<?php

namespace App\Http\Controllers;

use App\Interfaces\WebServiceInterface;
use App\Quote;
use App\Services\Soap;
use Illuminate\Http\Request;
use View;

class CompanyController extends Controller
{
    public $web_service;

    /**
     * This sets a WebServiceInterface compliant service at $this->webservice for the query operations.
     * This is resolved through the App\Providers\WebServiceProvider and the web_service.model
     *
     * @param WebServiceInterface $web_service
     */
    public function __construct(WebServiceInterface $web_service)
    {
        $this->web_service = $web_service;
    }


    /**
     *
     * Display a list of companies
     *
     * @return mixed
     */
    public function index()
    {
        $companies = [];

        try {
            $companies = $this->web_service->getCompanies();
        } catch (\Exception $e) {
            $this->handleException($e);
        }

        return View::make('company.index')->with([
            'companies' => $companies,
        ]);
    }


    /**
     * Provide a list of operations for a specific company
     *
     * @param $symbol
     * @return mixed
     */
    public function show($symbol)
    {
        try {
            $this->verifySymbol($symbol);

            return View::make('company.show')->with([
                'symbol' => $symbol
            ]);
        } catch (\Exception $e) {
            $this->handleException($e);
        }

        return redirect()->route('company.index');

    }


    /**
     * Checks that a symbol is exists in the output of getCompanies
     *
     * @param $symbol
     * @return bool
     * @throws \Exception
     */
    public function verifySymbol($symbol)
    {
        $companies = collect($this->web_service->getCompanies())->pluck('symbol')->flatten()->toArray();

        if (!in_array(strtoupper($symbol), $companies)) {
            throw new \Exception($symbol . ' doesn\'t seem to be a valid company symbol. Please try again');
        };

        return true;
    }


    /**
     * Displays the current quote for the company passed through the symbol
     *
     * @param $symbol
     * @return mixed
     */
    public function getQuote($symbol)
    {
        try {
            $this->verifySymbol($symbol);

            return View::make('company.get_quote')->with([
                'symbol' => strtoupper($symbol),
                'quote' => $this->web_service->getQuote($symbol),
                'quote_history' => Quote::orderBy('created_at', 'desc')->take(5)->get()
            ]);
        } catch (\Exception $e) {
            $this->handleException($e);
        }

        return redirect()->route('company.index');
    }


    /**
     * Saves a current quote price
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveQuote(Request $request)
    {
        try {
            $this->verifySymbol($request->get('symbol'));

            Quote::create([
                'symbol' => $request->get('symbol'),
                'price' => $request->get('price')
            ]);

            return redirect()->route('company.getQuote', ['symbol' => $request->get('symbol')]);


        } catch (\Exception $e) {
            $this->handleException($e);

        }

        return redirect()->route('company.index');

    }

    /**
     *
     *
     * @param $symbol
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function getDirectors($symbol)
    {
        try {
            return view('company.get_directors')->with([
                'symbol' => strtoupper($symbol),
                'directors' => $this->web_service->getDirectorsBySymbol($symbol)
            ]);
        } catch (\Exception $e) {
            $this->handleException($e);
        }

        return redirect()->route('company.index');
    }


}
