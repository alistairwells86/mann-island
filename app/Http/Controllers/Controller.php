<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function handleException(\Exception $e)
    {
        $message = $e->getMessage();
        if ($e->getPrevious()) {
            $previous_exception = $e->getPrevious();
            $message .= " - " . $previous_exception->getMessage();
        }
        flash($message, 'danger');
    }
}
