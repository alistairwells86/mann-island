<?php

namespace App\Interfaces;

/**
 * Interface OutputInterface
 * @package App\Interfaces
 *
 * This interface defines the methods to output the query results in a consistent manner.
 *
 */
interface OutputInterface
{
    /**
     * @param $data
     * @return array
     */
    public function outputCompanies($data);

    /**
     * @param $data
     * @return float
     */
    public function outputQuote($data);

    /**
     * @param $data
     * @return array
     */
    public function outputDirectorsBySymbol($data);
}