<?php

namespace App\Interfaces;

/**
 * Interface WebServiceInterface
 * @package App\Interfaces
 *
 * This interface defines the core methods of our Web Service
 *
 */
interface WebServiceInterface
{

    public function getCompanies();

    public function getQuote($symbol);

    public function getDirectorsBySymbol($symbol);
}