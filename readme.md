## Mann Island Code Test

Hi there everyone, hope you find my code easy enough to follow; if you have any questions please give me a shout on 07528 818439 or via alistairwells86@gmail.com

#### Installation

1. Clone down the repository with 

`git clone https://alistairwells86@gitlab.com/alistairwells86/mann-island.git`

2. Navigate to the directory and do a composer install - [Composer Download](https://getcomposer.org/download/)

3. Create a blank MySQL database and set the DB_DATABASE environment variable in .env (Note there may be other values required to be set depending on your setup)

4. From the project route run php artisan migrate to initialise the database

5. Ensure that SOAP_URL in .env matches the URL that the SOAP service will be provided from. You can see my config in the .env.example file

6. Configure your VM/web server to serve up the public directory of the project and navigate to the URL to begin making API calls.

#### Work Summary

* Made a Soap Service class at App\Services\Soap
* Created and applied a couple of interfaces to make sure any future web services provided the same methods and output their results in the same way 
* Made a service provider to dependency inject the a compatible WebService - the implementation can be swapped out in config.web_services.class
* Created a controller for routing the API calls
* Basic HTML views
* Basic database logic for saving quotes
* Various bits exception handling logic
* Little bit of validation

#### Future Scope

* Unit tests
* Integration tests
* Improved web interface 
* AJAX requests and view partials of results
* Singleton pattern for the web service to save quotes instead of hidden form fields

